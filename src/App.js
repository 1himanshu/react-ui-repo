import React from 'react';
import logo from './playstore-icon.png';
import './App.css';

function App() {
  
  var but ={
    backgroundColor:'orange',
    borderColor:'orange',
    borderradius:20,
    fontSize:30,
  }
 
  return (
    <div>
      <h1>
        Responsive UI 
      </h1>
      <img src={logo} alt="Logo"/>
      <button style ={but}>
          Login Screen
      </button>
    </div>
  );
  
}

export default App;
