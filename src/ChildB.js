import React, {Component} from 'react';

class B extends Component {
    constructor(props){
        super(props);
        console.log(props);
         this.state={
             message : 'Welcome here'
         }
    }

    change(){
        this.setState({
            message:"Thank you for clicking"
        })
    }

    render(){
    return (
        <div>
            <h2>child B</h2>
            {/* <input
                placeholder="your name"
                type= "text"
            /> */}
            {/* <h3>Hello {props.name}</h3>
            <h3>{props.text}</h3> */}
            <h2>{this.state.message}</h2>
            <button
            onClick = {() =>this.change()}>Click here</button>

        </div>
    );
    }
}
export default B;

