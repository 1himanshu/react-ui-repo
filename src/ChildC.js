import React from 'react'
import B from './ChildB';

export default function C() {
    return (
        <div>
             <h2>In child component C</h2>
            <B 
                name =  'HJ'
                text = ' prop passed'
            />
        </div>
    )
}
