import React from 'react'
import {BrowserRouter , Link , NavLink , Redirect, Prompt , Switch } from 'react-router-dom'                //using browser router
import {Route} from 'react-router-dom'

export default function Navigation() {                                                 //dafault component to navigate
    return (
  <BrowserRouter>
    <BaseLayout />
  </BrowserRouter>
 )
}


 const BaseLayout = () => (                                             //component for creating links and route
  <div>
    <header>
      <p>React Router v4 Browser Example</p>
        <nav>
          <ul>  
            <li><Link to='/'>Home</Link></li>                    
            <li><Link to='/about'>About</Link></li>
            <li><NavLink to='/me'
            activeStyle={
                {color:"green"}
            }
            >Profile</NavLink>
            </li>
            <li><Link to='/login'>Login</Link></li>
            <li><Link to='/register'>Register</Link></li>
            <li><Link to='/contact'>Contact</Link></li>
            <li><Link to='/users'>User</Link></li>
            
          </ul>
        </nav>
    </header>
    <div>
    <Switch>
      <Route path="/" exact component={HomePage} />
      <Route path="/about" exact component={AboutPage} />
      <Route path="/contact" exact component={ContactPage} />
      <Route path="/login" exact component={LoginPage} />
      <Route path="/register" exact component={RegisterPage} />                     
      <Route path="/me" exact strict render= {()=>{                              //render property
         return (<h1>My Profile</h1>)                                            //also using exact and strict property
      }} />
      <Route path="/users" component={UsersPage}/>
      
      {/* <Route  exact render= {()=>{                              
         return (<h1>Error 404!!!</h1>)
      }} /> */}
      </Switch>
    </div>
    
  </div>
)


const HomePage = () => <div>This is a Home Page</div>                      //other pages 
const LoginPage = () => <div>This is a Login Page</div>
const RegisterPage = () => <div>This is a Register Page</div>
const ProfilePage = () => <div>This is the Profile Page</div>
const AboutPage = () => <div>This is an About Page</div>
const ContactPage = () => <div>This is a Contact Page</div>

//nested routing 

const UsersPage = () => (
    <div>
    <h2>This is User's Page</h2>
    <ul>
        <li><Link to='/users/id/add'>Add</Link></li>
        <li><Link to='/users/id/edit'>Edit</Link></li>
    </ul>
    <div>
    <Switch>
        <Route path="/users/id/add" exact component={UsersAddPage} />
        <Route path="/users/id/edit" exact component={EditPage} />
      </Switch>
    </div>
    </div>
)

const UsersAddPage = () => <div>User's Add Page</div>
const EditPage = () => <div>User's Edit Page</div>

