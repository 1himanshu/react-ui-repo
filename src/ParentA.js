import React from 'react'
import B from './ChildB';
import C from './ChildC';

const A= () => 
        <div>
            <h1>Parent Component A</h1>
            
            <C/>
        </div>

export default A;